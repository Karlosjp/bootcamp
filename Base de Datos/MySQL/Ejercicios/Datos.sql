USE Autoescuela;

INSERT INTO Sala VALUES
('A01', 'Palomon', 5, 15),
('A02', 'Palomon', 5, 15),
('A03', 'Palomon', 5, 15),
('A04', 'Palomon', 5, 15);

INSERT INTO Alumno VALUES
('Alberto', 'Canarices', NULL, 'M', 10.25, 'A01', 'Calle palimpo' ),
('Julio', 'Canarices', NULL, 'M', 10.25, 'A02', 'Calle palimpo' ),
('Juan', 'Canarices', NULL, 'H', 10.25, 'A03', 'Calle palimpo' ),
('Alfredo', 'Canarices', NULL, 'H', 10.25, 'A02', 'Calle palimpo' ),
('Pepito', 'Canarices', NULL, 'H', 10.25, 'A04', 'Calle palimpo' ),
('Alberto', 'Canarices', NULL, 'M', 10.25, 'A03', 'Calle palimpo' ),
('Manolo', 'Canarices', NULL, 'H', 10.25, 'A02', 'Calle palimpo' ),
('Marta', 'Canarices', NULL, 'M', 10.25, 'A01', 'Calle palimpo' ),
('Alberto', 'Canarices', NULL, 'M', 10.25, 'A04', 'Calle palimpo' );
