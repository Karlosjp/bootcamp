DROP DATABASE IF EXISTS Autoescuela;

CREATE DATABASE Autoescuela;

USE Autoescuela;

CREATE TABLE Sala(
    CodSala CHAR(3),
    Nombre VARCHAR(15),
    Aforo SMALLINT UNSIGNED,
    Dimensiones SMALLINT,
    PRIMARY KEY (CodSala)
);

CREATE TABLE Alumno(
    CodAlumno SMALLINT AUTO_INCREMENT,
    Nombre VARCHAR(15),
    Fecha DATE,
    Genero CHAR(1),
    NotaSelect TINYINT UNSIGNED,
    CodigoSala CHAR(3),
    Direccion VARCHAR(20), 
    PRIMARY KEY (CodAlumno),
    FOREIGN KEY (CodigoSala) REFERENCES Sala (CodSala)
);