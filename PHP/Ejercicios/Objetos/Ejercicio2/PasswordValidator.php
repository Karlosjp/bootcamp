<?php

class PasswordValidator
{
    const CARACTERES_MINIMOS = ['.', ',', '-', '_', ',', ';'];

    public function isValid(string $contrasena)
    {
        /*$conteo = count($contrasena);
        $car = count($this->caracteres);
        if ($conteo >= 8)
            for ($i = 0; $i < $car; $i++) {
                for ($j = 0; $j < $conteo; $j++) {
                    if ($this->caracteres[$i] == $contrasena[$j]) {
                        $retorna = true;
                        break;
                    }
                }
            }*/
        return $this->longitudMinima($contrasena) && $this->caractererObligatorio($contrasena);
    }

    private function longitudMinima($contrasena)
    {
        $aux = false;
        if (strlen($contrasena) >= 8)
            $aux = true;

        return $aux;
    }

    private function caractererObligatorio($contrasena)
    {
        $aux = false;
        foreach (self::CARACTERES_MINIMOS as $caracter)
            if (strpos($contrasena, $caracter) !== false) {
                $aux = true;
                break;
            }

        return $aux;
    }
}
