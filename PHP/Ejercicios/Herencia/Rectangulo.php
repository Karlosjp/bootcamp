<?php

require_once 'Poligono.php';


class Rectangulo extends Poligono
{
    private $base;
    private $altura;

    public function __construct(float $base, float $altura)
    {
        $this->base = $base;
        $this->altura = $altura;
    }

    public function calcularArea()
    {
        return $this->altura * $this->base;
    }

}