<?php

require_once 'Poligono.php';


class Circulo extends Poligono
{
    private $radio;
    private $pi = "3.14";

    public function __construct(float $radio)
    {
        $this->radio = $radio;
    }

    public function calcularArea()
    {
        return $this->radio * $this->radio * $this->pi;
    }
}
