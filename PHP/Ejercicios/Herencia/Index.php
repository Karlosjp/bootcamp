<?php

require_once 'Cuadrado.php';
require_once 'Rectangulo.php';
require_once 'Circulo.php';

$dato1 = 10;
$dato2 = 3;

$cuadrado1 = new Cuadrado($dato1);
$circulo1 = new Cuadrado($dato1);
$rectangulo1 = new Cuadrado($dato1, $dato2);

echo "EL AREA DEL CUADRADO DE LADO $lado ES: " . $cuadrado1->calcularArea();
echo "EL AREA DEL CUADRADO DE LADO $lado ES: " . $circulo1->calcularArea();
echo "EL AREA DEL CUADRADO DE LADO $lado ES: " . $rectangulo1->calcularArea();
