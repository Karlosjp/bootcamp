<?php
/*  Crear directorio destino
    Copiar el fichero a un fichero llamado README destino dentro del del directorio Ejercicio_Ficheros
    Añadir al fichero nuestro nombre
*/

if (is_dir('./Destino') == false){
    mkdir('./Destino');
}

if(!file_exists('Destino/README-destino'))
    copy('Ejercicio_Fichero.txt', 'Destino/README-destino');

$fd = fopen('./Destino/README-destino', 'a+');
fwrite($fd, PHP_EOL . "Carlos");
fclose($fd);