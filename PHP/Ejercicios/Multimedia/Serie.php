<?php

require_once Temporada;
require_once Capitulo;
class Serie
{
    private $nombre;
    private $genero;
    private $temporadas;

    function __construct($nombre, $genero)
    {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->temporadas = [];
    }

    // Añade una nueva temporada a la serie
    public function anhadirTemporada(Temporada $temporada)
    {
        array_push($this->temporadas, $temporada);
    }

    // Devuelve el total de capitulos en una temporada
    public function totalCapitulos(Temporada $temporada): int
    {
        return $this->temporadas[$temporada]->totalCapitulos();
    }

    // Añade un capitulo a una temporada especifica de esta serie
    public function anhadirCapitulo(Capitulo $capitulo, Temporada $temporada)
    {
        $this->temporadas[$temporada]->anhadirCapitulo($capitulo);
    }

    // Devuelve la valoracion media de todas las temporadas
    public function valoracionMedia(): int
    {
        $aux = 0;
        foreach ($this->temporadas as $temporada)
            $aux += $temporada->valoracionMedia();

        $aux /= count($this->temporadas);

        return $aux;
    }
}
