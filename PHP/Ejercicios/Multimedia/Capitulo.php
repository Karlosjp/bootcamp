<?php

require_once Multimedia;

class Capitulo extends Multimedia
{
    private $titulo;
    private $fechaEstreno;
    private $valoracion;
    private $numeroCapitulo;

    function __construct($titulo, $fechaEstreno, $valoracion, $numeroCapitulo, $duracion)
    {
        $this->titulo = $titulo;
        $this->fechaEstreno = $fechaEstreno;
        $this->valoracion = $valoracion;
        $this->numeroCapitulo = $numeroCapitulo;
        $this->duracion = $duracion;
    }

    public function getValoracion(): float
    {
        return $this->valoracion;
    }
}
