<?php

require_once Capitulo;
class Temporada
{
    private $numTemporada;
    private $anho;
    private $capitulos;

    function __construct($numTemporada, $anho)
    {
        $this->numTemporada = $numTemporada;
        $this->anho = $anho;
        $this->capitulos = [];
    }

    // Añade un capitulo a la lista de capitulos.
    public function anhadirCapitulo(Capitulo $capitulo)
    {
        //if (is_object($capitulo) &&  $capitulo instanceof Capitulo)

        array_push($this->capitulos, $capitulo);
    }

    // Devuelve el total de capitulos en esta temporada
    public function totalCapitulos(): int
    {
        return count($this->capitulos);
    }

    // Devuelve la valoracion media de todos los capitulos de esta temporada
    public function valoracionMedia(): int
    {
        $aux = 0;
        foreach ($this->capitulos as $capitulo)
            $aux += $capitulo->getValoracion();

        $aux /= count($this->capitulos);

        return $aux;
    }
}
