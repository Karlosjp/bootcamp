<?php

require_once Serie;
require_once Capitulo;
require_once Temporada;

$cap = new Capitulo("pepe", "2019-10-01", 7.2, 1, 25);
$ser = new Serie("Alocadas aventuras", "comedia");
$tem = new Temporada(1, "2019");

$ser->anhadirTemporada($tem);
$ser->anhadirCapitulo($cap, $tem);

echo $ser->valoracionMedia($tem);
echo $ser->totalCapitulos($tem);
echo $ser->valoracionMedia();
