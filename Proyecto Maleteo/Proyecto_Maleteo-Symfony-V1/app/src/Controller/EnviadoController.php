<?php

namespace App\Controller;

use App\Entity\Comentarios;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EnviadoController extends AbstractController
{
    /**
     * @Route("/enviado", name="enviado")
     */

    public function enviado(EntityManagerInterface $em)
    {
        $repositorio = $em->getRepository(Comentarios::class);
        $comentarios = $repositorio->findAll();

        return $this->render('cuerpo/partes/formulario/enviado.html.twig',['comentarios' => $comentarios]);
    }
}