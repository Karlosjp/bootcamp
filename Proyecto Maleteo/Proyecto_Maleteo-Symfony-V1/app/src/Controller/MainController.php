<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Form\DemoFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, EntityManagerInterface $em, EntityManagerInterface $emComent)
    {
        $form = $this->createForm(DemoFormType::class);        
        $form->handleRequest($request);

        $repositorio = $emComent->getRepository(Comentarios::class);
        $comentarios = $repositorio->findAll();

        if($form->isSubmitted() && $form->isValid())
        {
            $demoSolicitud = $form->getData();
            
            $em->persist($demoSolicitud);
            $em->flush();
            
            return $this->redirectToRoute('enviado');
        }

        return $this->render('cuerpo/partes/formulario/formulario.html.twig',['formulario' => $form->createView(),'comentarios' => $comentarios]);
    }
}

    /**
     * @Route("/comentarios", name="listarComentarios")
     */
    /*
    public function listComentarios(EntityManagerInterface $em1,EntityManagerInterface $em2)
    {
        $repositorio = $em->getRepository(Comentarios::class);
        $comentarios = $repositorio->findAll();

        return $this->render('cuerpo/partes/opinion.html.twig', ['comentarios' => $comentarios]);
        
    }*/

    /**
     * @Route("/demo/nueva", name="nueva_demo")
     */
    /*public function nuevaDemo(Request $request)
    {
        $form = $this->createForm(DemoFormType::class);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            return $this->redirectToRoute('enviado');
        }

        return $this->render('cuerpo/partes/formulario/formulario.html.twig',['formulario' => $form->createView()]);
    }*/