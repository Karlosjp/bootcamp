<?php

namespace App\Controller;

use App\Entity\DemoSolicitud;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdministradorController extends AbstractController
{
    /**
     * @Route("/admin", name="administrar")
     */
    public function listSolicitudes(EntityManagerInterface $em1)
    {
        $repositorio = $em1->getRepository(DemoSolicitud::class);
        $solicitudes = $repositorio->findAll();

        return $this->render('admin/list.html.twig', ['solicitudes' => $solicitudes]);
        
    }
}