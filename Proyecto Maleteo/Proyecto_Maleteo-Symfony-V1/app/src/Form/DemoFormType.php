<?php

namespace App\Form;

use App\Entity\DemoSolicitud;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DemoFormType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre');
        $builder->add('email');
        $builder->add('horario');
        $builder->add('ciudad', ChoiceType::class,
        [
            'choices' =>
            [
                'Ciudad1' => 'c1',
                'Ciudad2' => 'c2',
                'Ciudad3' => 'c3',
                'Ciudad4' => 'c4'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => DemoSolicitud::class]);
    }
}