document.addEventListener("DOMContentLoaded", function() {
    console.log('Your document is ready!')

    var form_el = document.getElementById("myForm")
    var mensaje = document.getElementById("mensaje")
    var formulario = document.getElementById("contenidoForm")

    form_el.addEventListener("submit",
        function(evt) {
            evt.preventDefault();
            var fd = new FormData(form_el)
            var url = form_el.action
            var metodo = form_el.method

            fetch(url, {
                method: metodo,
                body: fd
            })

            .then(function(respuesta) {
                    respuesta.json()
                    console.log(respuesta)

                })
                .catch(function(respuesta) {
                    console.log(respuesta)
                })

            mensaje.className = "correcto"
            formulario.className = "oculto"
        })
})