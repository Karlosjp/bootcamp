/**
 * Ejercicio 5
 * Realiza una función que reciba un string y devuelva un objeto contando el número de apariciones
 * de cada letra en el string. Almacena y devuelve el resultado en un objeto.
 * Asegúrate de que la función cumple su comebdo haciendo uso de los tests aportados:
 */
var resultados = {}

// Separa un string en caracteres y cuenta cuantas hay
function contadorDeCaracteres(texto) {
    var arrayDeCaracteres = texto.split("")

    arrayDeCaracteres.forEach(actualizaContador);
    /*
    for (var l = 0; l < arrayDeCaracteres.length; l++) {
        actualizaContador(arrayDeCaracteres[l])
    }
    */
}

// Actualiza el array con los caracteres y su contador
var actualizaContador = function(letra) {
    if (resultados[letra]) {
        resultados[letra]++
    } else
        resultados[letra] = 1
}

function iniciar() {
    // Inicia el contador de caracteres y los guarda
    contadorDeCaracteres('carloscarcarloscas')

    // Escribe por consola todo el contenido del array de resultados
    for (var r in resultados)
        console.log("La letra " + r + " esta repetida: " + resultados[r])
}



/*

function contadorDeCaracteres(texto) {
    var arrayDeCaracteres = texto.split("")
    var resultados = {}
    var numCaracter = 0;
    var separadoCaracteres = separaLetras(arrayDeCaracteres)

    for (var l = 0; l < separadoCaracteres.length; l++) {
        for (var c = 0; c < arrayDeCaracteres.length; c++) {
            if (separadoCaracteres[l] == arrayDeCaracteres[c])
                numCaracter++
        }
        resultados[separadoCaracteres[l]] = numCaracter
    }
    return resultados
}

function separaLetras(letras) {
    var usadas = [letras[0]]
    estado = false;
    for (var l = 0; l < letras.length; l++) {
        for (var u = 0; u < usadas.length; u++)
            if (letras[l] != usadas[u])
                estado = true
            else {
                estado = false
                break
            }
        if (estado) {
            usadas.push(letras[l])
        }
    }
    return usadas
}
*/