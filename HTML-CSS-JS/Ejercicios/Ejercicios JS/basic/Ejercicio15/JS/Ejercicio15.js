/**
 * Crea una web que tenga dos columnas.
 * En la columna izquierda veremos un resumen de una cesta de la compra. Incluyendo nombre de
 * producto, cantidad y precio. La cesta deberá tener un total y mostrar su precio total con IVA.
 * Recuerda que debe haber un contador de carrito (carrito).
 * En la parte derecha tendremos un formulario que nos permita añadir carrito.
 */

document.addEventListener("DOMContentLoaded", function() {
    console.log('Your document is ready!');

    var form_el = document.getElementById("myForm");
    var nombre = document.getElementById("producto")
    var precio = document.getElementById("precio")
    var cantidad = document.getElementById("cantidad")
    var lista = document.getElementById("lista")
    var total = document.getElementById("total")
    var carrito = {}

    form_el.addEventListener("submit",
        function(evt) {
            evt.preventDefault();
            var producto = {}
            producto[nombre.name] = nombre.value
            producto[precio.name] = precio.value
            producto[cantidad.name] = cantidad.value

            // Si existe el producto, actualiza su cantidad y precio
            if (carrito[nombre.value])
                actualizarProductos(producto)
            else
                carrito[nombre.value] = producto

            // Vacia las cajas del formulario y el total = 0
            resetear()

            // Por cada producto en el carrito, escribe sus datos en la lista y calcula el total
            actualiza()
        })

    // Reinicia los campos
    function resetear() {
        nombre.innerHTML = ''
        precio.innerHTML = ''
        cantidad.innerHTML = ''
        total.innerHTML = 0
        lista.innerHTML = ''
    }

    // Por cada producto en el carrito, escribe sus datos en la lista y calcula el total
    function actualiza() {
        for (var produc in carrito) {
            var listado = document.createElement('li')
            listado.innerHTML = "Producto: " + carrito[produc]['nombre'] + " | Cantidad: " + carrito[produc]['cantidad'] +
                " --> total: " + (parseInt(carrito[produc]["precio"]) * parseInt(carrito[produc]['cantidad']))
            lista.appendChild(listado)
            calcular(carrito[produc]);
        }
    }

    // Actualiza las cantidades y los precios
    var actualizarProductos = function(producNuevo) {
        carrito[producNuevo["nombre"]]['cantidad'] = parseInt(carrito[producNuevo["nombre"]]['cantidad']) + parseInt(producNuevo["cantidad"])
    }

    // Calcula el total en el carrito
    var calcular = function(producto) {
        total.innerHTML = "Total en carrito: " + (parseInt(producto["precio"]) + parseInt(total.innerHTML)) * parseInt(producto['cantidad'])
    }
});