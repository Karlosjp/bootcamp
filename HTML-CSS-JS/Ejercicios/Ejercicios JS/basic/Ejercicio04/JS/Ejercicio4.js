/**
 * Vamos a complicar el ejercicio anterior:
 * Ahora cada vez que calculemos la longitud del string más largo, almacenaremos el resultado en un array de resultados.
 * Una vez ejecutados varios cálculos de longitud (4 en el ejemplo), vamos a calcular la media de los resultados.
 */
var resultados = []
var larga = ""
var llamada = 0
var arrayDeTest1 = ["Richie", "Joanie", "Greg", "Marcia", "Bobby"]
var arrayDeTest2 = ["Blanka", "Zangief", "Chun Li", "Guile"]
var arrayDeTest3 = ["Red", "Blue", "Green"]

function calculoLongitudMasLargo(test) {

    for (var indice in test)
        if (test[indice].length > larga.length)
            larga = test[indice]

    resultados[llamada] = larga
    llamada++
    larga = ""
}

function media(resultados) {
    var media = 0.

    for (var indice in resultados)
        media += resultados[indice].length

    return (media / llamada)
}

calculoLongitudMasLargo(arrayDeTest1)
calculoLongitudMasLargo(arrayDeTest2)
calculoLongitudMasLargo(arrayDeTest3)

console.log(media(resultados))