/**
 * Ejercicio 9: Listado de Pokemons con ES6
 * Dispones de la API de pokemons ofrecida por https://pokeapi.co/
 * Haciendo uso de Fetch, promesas y clases haz una petición que recupere una lista de pokemons
 * (https://pokeapi.co/api/v2/pokemon/) y los pinte en pantalla.
 * En la respuesta podrás ver que cada pokemon tiene una URL propia
 * (https://pokeapi.co/api/v2/pokemon/3/). Haz que al clicar en el nombre de un pokemon, te pinte 
 * sus detalles en la parte de la derecha. Verás que hay muchos datos… basta con que pintes su
 * imagen, su nombre, su peso y su altura.
 * NOTA: la imagen está en sprites -> front_default
 */


document.addEventListener("DOMContentLoaded", function() {
    var myTemplatePokemon = "<p id='{{pokemon}}'>" +
        "Pokemon: {{pokemon}}" +
        "</p>"
    var myTemplateDatos = "<img id='img{{pokemon}}' src='{{sprite}}'>" +
        "<p id='datos{{pokemon}}'>" +
        "Pokemon: {{pokemon}}," +
        " Peso: {{peso}}," +
        " Altura: {{altura}}" +
        "</p>"
    class Pokemon {
        constructor(url, nombre) {
            this.nombre = nombre
            this.url = url
            this.peso = 0
            this.altura = 0
            this.imagen = ""
        }
        getNombre() {
            return this.nombre
        }

        getPeso() {
            return this.peso
        }
        setPeso(peso) {
            this.peso = peso
        }
        getAltura() {
            return this.altura
        }
        setAltura(altura) {
            this.altura = altura
        }
        getImagen() {
            return this.imagen
        }
        setImagen(imagen) {
            this.imagen = imagen
        }
        getUrl() {
            return this.url
        }

        // Devuelve Nombre, Imagen, Peso y Altura del Pokemon
        devolverNIPA() {
            return {
                pokemon: this.nombre,
                sprite: this.imagen,
                peso: this.peso,
                altura: this.altura
            }
        }

        // Devuelve Nombre del Pokemon
        devolverN() {
            return {
                pokemon: this.nombre
            }
        }
    }

    var lPokemon = 'https://pokeapi.co/api/v2/pokemon/'
    var listaPokemon = {}

    buscar()

    function buscar() {
        fetch(lPokemon)
            .then(function(response) {
                return response.json();
            })
            .then(function(obj) {
                var pokemons = obj.results.map(function(pokemon) {
                    return pokemon
                });
                pokemons.forEach(element => {
                    listaPokemon[element.name] = new Pokemon(element.url, element.name)
                    completar(element.url, element.name)
                });
                rellenar()
            })
            .catch(function(err) {
                console.error(err);
            });

    }

    function completar(url, name) {
        fetch(url)
            .then(function(response) {
                return response.json();
            })
            .then(function(obj) {
                listaPokemon[name].altura = obj.height
                listaPokemon[name].peso = obj.weight
                listaPokemon[name].imagen = obj.sprites.front_default
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    function rellenar() {
        var cuerpo = document.getElementById("cuerpo")
        var listado = document.createElement('ol')
        listado.id = "listado"
        cuerpo.appendChild(listado)

        for (var pk in listaPokemon) {
            let elemento = document.createElement('li')
            let result = Sqrl.Render(myTemplatePokemon, listaPokemon[pk].devolverNIPA())

            elemento.addEventListener("click", mostrarDatos)
            listado.appendChild(elemento)
            elemento.innerHTML = result
        }

    }

    var mostrarDatos = function() {
        var result = Sqrl.Render(myTemplateDatos, listaPokemon[this.firstChild.id].devolverNIPA())
        var informacion = document.getElementById("datos")

        informacion.innerHTML = result
    }
})