/**
 * Ejercicio 9: Listado de Pokemons con ES6
 * Dispones de la API de pokemons ofrecida por https://pokeapi.co/
 * Haciendo uso de Fetch, promesas y clases haz una petición que recupere una lista de pokemons
 * (https://pokeapi.co/api/v2/pokemon/) y los pinte en pantalla.
 * En la respuesta podrás ver que cada pokemon tiene una URL propia
 * (https://pokeapi.co/api/v2/pokemon/3/). Haz que al clicar en el nombre de un pokemon, te pinte 
 * sus detalles en la parte de la derecha. Verás que hay muchos datos… basta con que pintes su
 * imagen, su nombre, su peso y su altura.
 * NOTA: la imagen está en sprites -> front_default
 */


document.addEventListener("DOMContentLoaded", function() {

    /**
     * Clase pokemon 
     */
    class Pokemon {
        constructor(url, nombre) {
            this.nombre = nombre
            this.url = url
            this.peso = 0
            this.altura = 0
            this.imagen = ""
        }
        getNombre() {
            return this.nombre
        }

        getPeso() {
            return this.peso
        }
        setPeso(peso) {
            this.peso = peso
        }
        getAltura() {
            return this.altura
        }
        setAltura(altura) {
            this.altura = altura
        }
        getImagen() {
            return this.imagen
        }
        setImagen(imagen) {
            this.imagen = imagen
        }
        getUrl() {
            return this.url
        }
    }

    var lPokemon = 'https://pokeapi.co/api/v2/pokemon/'
        // Objeto donde se guardaran todos los pokemons de la busqueda
    var listaPokemon = {}

    // Cuando se cargue la pagina, inicia la funcion buscar
    buscar()

    /**
     * Recoge el resultado del fetch en el objeto listaPokemon
     */
    function buscar() {
        fetch(lPokemon)
            .then(function(response) {
                return response.json();
            })
            .then(function(obj) {
                var pokemons = obj.results.map(function(pokemon) {
                    return pokemon
                });
                pokemons.forEach(element => {
                    listaPokemon[element.name] = new Pokemon(element.url, element.name)
                    completar(element.url, element.name)
                });
                rellenar()
            })
            .catch(function(err) {
                console.error(err);
            });

    }

    /**
     * Recoge los datos faltantes del pokemon pasado por parametro
     */
    function completar(url, name) {
        fetch(url)
            .then(function(response) {
                return response.json();
            })
            .then(function(obj) {
                listaPokemon[name].altura = obj.height
                listaPokemon[name].peso = obj.weight
                listaPokemon[name].imagen = obj.sprites.front_default
            })
            .catch(function(err) {
                console.error(err);
            });
    }

    /**
     * Crea una lista de los pokemons guardados en listaPokemon
     */
    function rellenar() {
        var cuerpo = document.getElementById("cuerpo")
        var listado = document.createElement('ol')
        listado.id = "listado"
        cuerpo.appendChild(listado)

        for (var pk in listaPokemon) {
            let elemento = document.createElement('li')
            let nPokemon = document.createElement('p')
            nPokemon.id = pk
            nPokemon.setAttribute("Buscar", pk)
            nPokemon.innerHTML = pk
            nPokemon.addEventListener("click", mostrarDatos)
            elemento.appendChild(nPokemon)
            listado.appendChild(elemento)

        }

    }

    /**
     * Al hacer click, muestra todos los datos del pokemon en la pante derecha
     */
    var mostrarDatos = function() {
        var informacion = document.getElementById("datos")
        var imagen = document.createElement('img')
        var datos = document.createElement('p')
        datos.innerHTML =
            "Nombre: " + this.id +
            ", Peso: " + listaPokemon[this.id].getPeso() +
            ", Altura: " + listaPokemon[this.id].getAltura()
        imagen.src = listaPokemon[this.id].getImagen()
        informacion.appendChild(imagen)
        informacion.appendChild(datos)
    }
})