/**
 * Ejercicio 2: Calculadora con Clases V2
 * Haciendo uso de la calculadora del ejercicio anterior, realiza dos nuevas calculadoras que hereden
 * de la calculadora base:
 * 
 * ● Calculadora “Cutronga”: Esta calculadora será invocada desde la consola, mostrará los
 * resultados de las operaciones desde alertas de JS y pedirá los valores mediante prompts.
 * 
 * ● Calculadora Visual: Esa calculadora al iniciarse pintará en el HTML botones con las acciones
 * disponibles, un pequeño display con el resultado y un input para meter los valores.
 * 
 * 
 * Recuerda que debes hacer que la herencia tenga sentido. Para ello ambas calculadoras usarán los
 * métodos sumar, restar, dividir… etc de su padre. NO IMPLEMENTES TODOS
 * 
 * AYUDITA: Quizá solo tengas que sobrescribir el constructor, la función pedir valor y pintar valor
 */
document.addEventListener("DOMContentLoaded", function() {
    console.log('Your document is ready!');
    class Calculadora {

        constructor() {
            this.n1 = 0
            this.n2 = 0
            this.resultados = []
        }

        getN1() {
            return this.n1
        }

        getN2() {
            return this.n2
        }

        introducir(n) {
            this.n1 = this.n2
            this.n2 = n
        }

        suma() {
            this.resultados.push(this.n1 + this.n2)
            return this.n1 + this.n2;
        }

        resta() {
            this.resultados.push(this.n1 - this.n2)
            return this.n1 - this.n2
        }

        division() {
            this.resultados.push(this.n1 / this.n2)
            return this.n1 / this.n2
        }

        multiplicación() {
            this.resultados.push(this.n1 * this.n2)
            return this.n1 * this.n2
        }

        potencia() {
            var aux = Math.pow(this.n1, this.n2)
            this.resultados.push(aux)
            return aux
        }
        limpiar() {
            this.resultados.splice(0)
        }
    }
    class CalculadoraCutrona extends Calculadora {

        constructor() {
            super()
        }

        introducir() {
            super.introducir(parseInt(prompt("Escribe un numero")))
            super.introducir(parseInt(prompt("Escribe un numero")))
        }

        sacaporpantalla(tipo, numero) {
            alert("La " + tipo + " de " + this.n1 + " y " + this.n2 + " es: " + numero);
        }

        suma() {
            this.sacaporpantalla("suma", super.suma())
        }

        resta() {
            this.sacaporpantalla("resta", super.resta())
        }

        division() {
            this.sacaporpantalla("division", super.division())
        }

        multiplicación() {
            this.sacaporpantalla("multiplicación", super.multiplicación())
        }

        potencia() {
            this.sacaporpantalla("potencia", super.potencia())
        }

        limpiar() {
            super.limpiar()
        }
    }
    class CalculadoraVisual extends Calculadora {

        constructor() {
            super()
        }

        crearVisual() {
            var form_el = document.getElementById("myForm");
            for (var i = 0; i < 2; i++) {
                var nc = document.createElement('input')
                nc.setAttribute("type", "text")
                form_el.appendChild(nc)
            }
        }
    }
    /*
        introducir() {
            super.introducir(parseInt(prompt("Escribe un numero")))
            super.introducir(parseInt(prompt("Escribe un numero")))
        }

        sacaporpantalla(tipo, numero) {
            alert("La " + tipo + " de " + this.n1 + " y " + this.n2 + " es: " + numero);
        }

        suma() {
            this.sacaporpantalla("suma", super.suma())
        }

        resta() {
            this.sacaporpantalla("resta", super.resta())
        }

        division() {
            this.sacaporpantalla("division", super.division())
        }

        multiplicación() {
            this.sacaporpantalla("multiplicación", super.multiplicación())
        }

        potencia() {
            this.sacaporpantalla("potencia", super.potencia())
        }

        limpiar() {
            super.limpiar()
        }
    }
    /*/
    var cal = new CalculadoraVisual()

    cal.crearVisual()

});