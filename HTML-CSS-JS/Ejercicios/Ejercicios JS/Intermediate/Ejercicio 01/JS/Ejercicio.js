/**
 * Ejercicio 1: Calculadora con Clases (SOLO JavaScript)
 * Realiza una clase calculadora que tenga los siguientes métodos:
 * - Introducir valor
 * - Suma
 * - Resta
 * - División
 * - MulGplicación
 * - Potencia
 * - Limpiar
 * Instancia la calculadora y realiza algunos cálculos desde la consola para comprobar que funciona
 * correctamente. La calculadora deberá acumular los cálculos hasta que se limpie.
 */
class Calculadora {

    constructor() {
        this.n1 = 0
        this.n2 = 0
        this.resultados = []
    }

    getN1() {
        return this.n1
    }

    getN2() {
        return this.n2
    }

    introducir(n) {
        this.n1 = this.n2
        this.n2 = n
    }

    suma() {
        this.resultados.push(this.n1 + this.n2)
        return this.n1 + this.n2;
    }

    resta() {
        this.resultados.push(this.n1 - this.n2)
        return this.n1 - this.n2
    }

    division() {
        this.resultados.push(this.n1 / this.n2)
        return this.n1 / this.n2
    }

    multiplicación() {
        this.resultados.push(this.n1 * this.n2)
        return this.n1 * this.n2
    }

    potencia() {
        var aux = Math.pow(n1, n2)
        this.resultados.push(aux)
        return aux
    }
    limpiar() {
        this.resultados.splice(0)
    }
}

var cal = new Calculadora()

cal.introducir(45)
cal.introducir(2)

console.log("La Suma de " + cal.getN1() + " + " + cal.getN2() + " es: " + cal.suma())
console.log("La Resta de " + cal.getN1() + " - " + cal.getN2() + " es: " + cal.resta())
console.log("La División de " + cal.getN1() + " / " + cal.getN2() + " es: " + cal.division())
console.log("La Multiplicación de " + cal.getN1() + " * " + cal.getN2() + " es: " + cal.multiplicación())
console.log("La Potencia de " + cal.getN1() + " ^ " + cal.getN2() + " es: " + cal.potencia())
cal.limpiar()