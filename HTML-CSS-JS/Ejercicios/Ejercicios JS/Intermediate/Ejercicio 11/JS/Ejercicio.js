/**
 * Dispones de dos peticiones, la primera te devuelve un listado de álbumes:
 * https://jsonplaceholder.typicode.com/albums
 * Y la segunda de te devuelve un listado de fotos:
 * https://jsonplaceholder.typicode.com/photos
 * Haz una vista haciendo uso de Fetch, Promesas y Clases, que te pinte un listado de álbumes.
 * Cuando hagas click en uno de ellos, se deberá hacer una petición a las fotos y pintar en un
 * pequeño muro estilo Instagram, las que correspondan a ese álbum.
 * Deberás hacer tú el filtrado dependiendo del parámetro albumId de cada foto.
 */
import Photo from "./photos.js"
import Albunes from "./Albunes.js"

document.addEventListener("DOMContentLoaded", function() {


    const photos = 'https://jsonplaceholder.typicode.com/photos'

    const albunes = new Albunes()

    buscar()

    function buscar() {
        fetch(photos)
            .then(function(response) {
                return response.json();
            })
            .then(function(obj) {
                for (var photo in obj)
                    albunes.agregarPhoto(obj[photo])
                rellenar()
            })

        .catch(function(err) {
            console.error(err);
        })
    }

    function rellenar() {
        var cuerpo = document.getElementById("album")
        var listado = document.createElement('ul')
        listado.id = "listado"
        cuerpo.appendChild(listado)

        for (var album in albunes) {
            let elemento = document.createElement('li')
            let result = Sqrl.Render(myTemplateAlbum, albunes[album])

            elemento.addEventListener("click", mostrarDatos)
            listado.appendChild(elemento)
            elemento.innerHTML = result
        }

    }
    /*
        var myTemplatePokemon = "<p id='{{pokemon}}'>" +
            "Pokemon: {{pokemon}}" +
            "</p>"
        var myTemplateDatos = "<img id='img{{pokemon}}' src='{{sprite}}'>" +
            "<p id='datos{{pokemon}}'>" +
            "Pokemon: {{pokemon}}," +
            " Peso: {{peso}}," +
            " Altura: {{altura}}" +
            "</p>"
        class Album {
            constructor(id) {
                this.id = id
                this.fotos = {}
            }
            getId() {
                return this.id
            }
        }*/
})