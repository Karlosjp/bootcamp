class Photos {
    constructor(title, url, thumbnailUrl) {
        this.title = title
        this.url = url
        this.thumbnailUrl = thumbnailUrl
    }

    devolverData() {
        return {
            title: this.title,
            url: this.url,
            thumbnailUrl: this.thumbnailUrl
        }
    }
}

export default Photos