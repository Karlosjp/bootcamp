import Photo from "./photos.js"
class Albunes {

    constructor() {
        this.lista = {}
    }

    agregarPhoto(photo) {
        if (this.lista[photo.albumId])
            this.crearPhoto(photo)
        else {
            this.lista[photo.albumId] = {}
            this.crearPhoto(photo)
        }
    }

    crearPhoto(photo) {
        this.lista[photo.albumId][photo.id] = new Photo(photo.title, photo.url, photo.thumbnailUrl)
    }
}

export default Albunes