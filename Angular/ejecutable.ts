
let unaVariable= "22000";
let varTexto = "un texto";

console.log("texto: " + varTexto);
console.log("texto: " + unaVariable);

class Pokemon {
    protected especie: string;
    protected altura: number;
    public evolucionado = false;

    constructor(especie, altura) {
        this.especie = especie;
        this.altura = altura;
    }
    informa(){
        console.log(`Especie: ${this.especie} altura: ${this.altura}`);
    }
    toString(){
        return `Especie: ${this.especie} altura: ${this.altura}`;
    }
}

let bulbasur = new Pokemon ("Bulbasur", 99);

bulbasur.informa();

class PokemonEvolucionado extends Pokemon{
    constructor(especie:string, altura :number){
        super(especie, altura);
        this.evolucionado = true;
    }
    informa(){
        console.log(`Especie evolucionada: ${this.especie} altura: ${this.altura}`);
    }
    toString(){
        return `Especie evolucionada: ${this.especie} altura: ${this.altura}`;
    }
}

let charizard : Pokemon = new PokemonEvolucionado("Charizard", 150);
charizard.informa();


let charizardPadre : Pokemon = charizard;
