import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'comp-una-funcion',
  templateUrl: './una-funcion.component.html',
  styleUrls: ['./una-funcion.component.css']
})
export class UnaFuncionComponent implements OnInit {

  titulo: string = "UnaFuncionComponent";
  
  constructor() { }

  ngOnInit() {
  }

}
