var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var unaVariable = "22000";
var varTexto = "un texto";
console.log("texto: " + varTexto);
console.log("texto: " + unaVariable);
var Pokemon = /** @class */ (function () {
    function Pokemon(especie, altura) {
        this.evolucionado = false;
        this.especie = especie;
        this.altura = altura;
    }
    Pokemon.prototype.informa = function () {
        console.log("Especie: " + this.especie + " altura: " + this.altura);
    };
    Pokemon.prototype.toString = function () {
        return "Especie: " + this.especie + " altura: " + this.altura;
    };
    return Pokemon;
}());
var bulbasur = new Pokemon("Bulbasur", 99);
bulbasur.informa();
var PokemonEvolucionado = /** @class */ (function (_super) {
    __extends(PokemonEvolucionado, _super);
    function PokemonEvolucionado(especie, altura) {
        var _this = _super.call(this, especie, altura) || this;
        _this.evolucionado = true;
        return _this;
    }
    PokemonEvolucionado.prototype.informa = function () {
        console.log("Especie evolucionada: " + this.especie + " altura: " + this.altura);
    };
    PokemonEvolucionado.prototype.toString = function () {
        return "Especie evolucionada: " + this.especie + " altura: " + this.altura;
    };
    return PokemonEvolucionado;
}(Pokemon));
var charizard = new PokemonEvolucionado("Charizard", 150);
charizard.informa();
