import { Component, OnInit } from '@angular/core';
import { Solicitud } from '../modelo/Solicitud';
import { SolicitudesService } from '../solicitudes.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  nuevaSolicitud: Solicitud;
  politicaAceptar: boolean;

  constructor(private srvSolicitudes : SolicitudesService) { }

  ngOnInit() {
    this.politicaAceptar = true;
    this.nuevaSolicitud = new Solicitud("pruebasdfasdfasdfa", "", "", "");
  }
  enviar(){
    //alert(this.nuevaSolicitud.getNombre());
    this.srvSolicitudes.enviar(this.nuevaSolicitud);
  }

}
