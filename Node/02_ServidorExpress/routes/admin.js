const router = require('express').Router()

router.get('/add-product', (req, res, next) => {
    res.send('<form action="/admin/add-product" method="POST"><input type="text" name="product"><button type="submit">Add Product</button></form>')
})

router.post('/add-product', (req, res, next) => {
    console.log('PRODUCTO', req.body);
    res.redirect('/')
})

module.exports = router