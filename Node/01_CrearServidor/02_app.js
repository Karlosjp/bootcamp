const http = require('http');

const server = http.createServer((req, res) => {

  if(req.url==='/contacto'){
    res.end('Soy la pagina de contacto');
  } else {
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>Mi Primera Pagina Node</title></head>');
    res.write('<body><h1>Hola mundo desde mi servidor NodeJS!</h1></body>');
    res.write('</html>');
    res.end();
  }
});

server.listen(3000);