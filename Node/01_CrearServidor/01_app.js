const http = require('http');

function serverReqFn(req, res){
  console.log('Hola desde el servidor');
}

const server = http.createServer(serverReqFn);
server.listen('3000', ()=>console.log('Servidor escuchando el puerto 3000'));