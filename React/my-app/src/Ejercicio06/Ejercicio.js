import React, { Component } from 'react'
import { Contador } from '../Ejercicio04/Contador';

/**
 * Ejercicio 5
 * 
 * Partiendo del componente contador del ejercicio 4, crea un componente que al hacer clic sobre
 * un botón añada dinámicamente contadores:
 *  ● Cada contador tiene que funcionar de manera independiente
 *  ● Añade un botón a los contadores para que pueda reducir el valor del contador
 */

export class Ejercicio06 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            numContadores: 0,
            contadores: []

        }
        this.contar = this.contar.bind(this);
    }

    render() {
        return (
            <div>
                <button onClick={this.contar}>
                    Agregar nuevo contador
                </button>
                <p>
                    {this.state.contadores}
                </p>
            </div>)
    }

    contar() {
        // Creo nuevo componente contador para el array contadores
        let nuevoContador = <Contador key={this.state.numContadores} />
        let auxContadores = this.state.contadores
        auxContadores.push(nuevoContador)

        this.setState(
            {
                numContadores: this.state.numContadores + 1,
                contadores: auxContadores
            }
        )
    }
}

