import React from 'react';
import logo from './logo.svg';
import './App.css';
import HolaMundo from './Ejercicio01/HolaMundo';
import HolaMundo_2 from './Ejercicio02/HolaMundo_2';
import Header from './Ejercicio03/Header';
import Footer from './Ejercicio03/Footer';
import Main from './Ejercicio03/Main/Main/Main';
import Nav from './Ejercicio03/Main/Nav/Nav';
import Aside from './Ejercicio03/Main/Aside/Aside';
import {Contador} from './Ejercicio04/Contador';
import { Reloj } from './Ejercicio05/Reloj';
import {Ejercicio06} from './Ejercicio06/Ejercicio';

/**
 * Ejercicio 5
 * 
 * Partiendo del componente contador del ejercicio 4, crea un componente que al hacer clic sobre
 * un botón añada dinámicamente contadores:
 *  ● Cada contador tiene que funcionar de manera independiente
 *  ● Añade un botón a los contadores para que pueda reducir el valor del contador
 */
function App() {
  return (
    <div className="App">
      <Ejercicio06/>
    </div>
  );
}

/**
 * Modifica el componente App para que muestre una estructura de componentes como la de la
 * imagen:
 */
/*
function App() {
  return (
    <div className="App">
      <p>
        App
      </p>
      <Header />
      <div className="cTipo1 grupoTipo1">
        <Nav />
        <Main />
        <Aside  />
      </div>
      <Footer />
    </div>
  );
}
*/

/**
 * Ejercicio 1:
 * Utilizando el esqueleto de la aplicación generado con Create React App:
 * ● Elimina el componente creado por defecto
 * ● Crea uno nuevo que pinte un mensaje “Hola mundo”
 */
/**
  * function App() {
  *   return (
  *     <div className="App">
  *       <HolaMundo/>
  *     </div>
  *   );
  * }
  */
/**
 *  Ejercicio 2:
 * Utilizando el código del ejercicio anterior:
 * 1. Modifica el componente para que acepte una prop con un nombre
 * 2. Modifica el componente para que acepte un objeto user con
 *   ○ name
 *   ○ surname
 */
/*
function App() {
  const user = {name:"Carlos", surname:"Jaquez"}
  return (
       <div className="App">
         <HolaMundo_2 user={user}/>
       </div>
     );
}
*/

/*function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}*/

/**
 * Ejercicio 4
 * Crea un componente contador que muestre los clicks realizados sobre un botón del propio componente
 * Cada click debe aumentar en 1 el valor del contador, almacenando el valor en su estado
 */
/*
function App() {
  const cont = Contador
  return (
    <div className="App">
      <Contador/>
    </div>
  );
}
*/

/**
 * Ejercicio 5
 * 
 * Crea un componente reloj
 *  ● Guarda la hora en el estado interno
 *  ● Actualiza la hora cada segundo
 */
/*
function App() {
  return (
    <div className="App">
      <Reloj/>
    </div>
  );
}*/
export default App;