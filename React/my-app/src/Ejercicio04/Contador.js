import React, { Component } from 'react'

/**
 * Ejercicio 4
 * Crea un componente contador que muestre los clicks realizados sobre un botón del propio componente
 * Cada click debe aumentar en 1 el valor del contador, almacenando el valor en su estado
 */

export class Contador extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contador: 0
        }
        this.contar = this.contar.bind(this);
    }

    render() {
        return (
            <div>
                <button onClick={this.contar}>
                    Contador
                </button>
                <p>{this.state.contador}</p>
            </div>)
    }

    contar() {
        this.setState(
            {
                contador: this.state.contador + 1
            })
    }
}

