import React, { Component } from 'react'
import Footer from "./Footer"
import Aside from "./Aside"

class Main extends Component {
    render() {
        return (
            <div className="caja cTipo1 aTipo2" >
                <p>
                    Main
                </p>
                <Aside />
                <Footer />
            </div>
        )
    }
}
export default Main