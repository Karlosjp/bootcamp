import React, { Component } from 'react'

/**
 * Ejercicio 5
 * 
 * Crea un componente reloj
 *  ● Guarda la hora en el estado interno
 *  ● Actualiza la hora cada segundo
 * 
 * 
 *  this.is.magic
 */

export class Reloj extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fecha: new Date()
        }

        /**
         * Como hacerlo forma normal
         */
        /*this.timerID = setInterval(
            () => this.tick(),
            1000
        )*/
    }

    /**
     * Como hacerlo forma "fina"
     */
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        )
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            fecha: new Date()
        })
    }

    render() {
        return (
            <div>
                <p>
                    {this.state.fecha.getHours()} : {this.state.fecha.getMinutes()} : {this.state.fecha.getSeconds()}
                </p>
            </div>
        )
    }
}
